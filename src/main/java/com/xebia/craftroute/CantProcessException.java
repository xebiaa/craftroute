package com.xebia.craftroute;

public class CantProcessException extends Exception {

	public CantProcessException(Exception e) {
		super(e);
	}

}
