package com.xebia.craftroute;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.cyberneko.html.parsers.DOMParser;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.inject.Singleton;

@Singleton
public class ConfluencePageTransformer {

	private static final String BASE_URL = "https://projects.xebia.com/confluence/display/CRAFT/";
	private Transformer transformer;
	private DOMParser parser;

	public ConfluencePageTransformer() {
		parser = new DOMParser();
		TransformerFactory transFact = TransformerFactory.newInstance();
		Source xlstSource = new StreamSource(ConfluencePageTransformer.class.getClassLoader().getResourceAsStream("transform.xsl"));
		try {
			transformer = transFact.newTransformer(xlstSource);
		} catch (TransformerConfigurationException tce) {
			// Mayday, mayday...
			throw new RuntimeException(tce);
		}
	}

	public String grabAndTransform(String id) throws CantProcessException {
		try {
			return transform(grab(new URL(BASE_URL + id)));
		} catch (Exception e) {
			throw new CantProcessException(e);
		}
	}

	public DOMSource grab(URL url) throws IOException, SAXException {
		// Until I figure out how to circumvent Confluence login...
		// parser.parse(new InputSource(url.openStream()));
		// stubbin'
		parser.parse(new InputSource(getClass().getClassLoader().getResourceAsStream("goodcitizen-runtime.html")));
		Document document = parser.getDocument();
		return new DOMSource(document);
	}

	public String transform(DOMSource domSource) throws TransformerException {
		StringWriter stringWriter = new StringWriter();
		Result result = new StreamResult(stringWriter);
		transformer.transform(domSource, result);
		return stringWriter.toString();
	}

}
