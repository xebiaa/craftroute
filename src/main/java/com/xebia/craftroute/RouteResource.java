package com.xebia.craftroute;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.sun.jersey.api.NotFoundException;

@Path("/route")
public class RouteResource {

	private final ConfluencePageTransformer confluencePageTransformer;

	@Inject
	public RouteResource(ConfluencePageTransformer confluencePageTransformer) {
		this.confluencePageTransformer = confluencePageTransformer;
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.TEXT_HTML)
	public String route(@PathParam("id") String id) {
		if (Strings.isNullOrEmpty(id)) {
			throw new NotFoundException();
		}
		try {
			return confluencePageTransformer.grabAndTransform(id);
		} catch (CantProcessException e) {
			throw new WebApplicationException(); // default to 500.
		}
	}
}
