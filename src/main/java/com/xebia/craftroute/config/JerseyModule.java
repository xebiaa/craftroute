package com.xebia.craftroute.config;

import java.util.HashMap;
import java.util.Map;

import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import com.xebia.craftroute.ConfluencePageTransformer;
import com.xebia.craftroute.RouteResource;

public class JerseyModule extends JerseyServletModule {

	@Override
	protected void configureServlets() {
		bind(RouteResource.class);
		bind(ConfluencePageTransformer.class);
		// Jersey Configuration
		Map<String, String> params = new HashMap<String, String>();
		params.put(ResourceConfig.FEATURE_DISABLE_WADL, "true");
		params.put(ResourceConfig.FEATURE_REDIRECT, "true");
		filter("/*").through(GuiceContainer.class, params);
	}

}
