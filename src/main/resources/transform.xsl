<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
		   <head><title></title></head>
			<body>
			   <xsl:copy-of select="HTML/BODY/DIV/DIV/DIV/DIV/DIV/DIV[@id='card_images']"/>
			   <xsl:copy-of select="HTML/BODY/DIV/DIV/DIV/DIV/DIV/H3|HTML/BODY/DIV/DIV/DIV/DIV/DIV/P[not(@class='noprint')]"/>
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>