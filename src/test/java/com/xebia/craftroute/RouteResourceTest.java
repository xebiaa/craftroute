package com.xebia.craftroute;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import javax.ws.rs.WebApplicationException;

import org.junit.Before;
import org.junit.Test;

import com.sun.jersey.api.NotFoundException;

public class RouteResourceTest {

	private static final String SOME_HTML = "<html><body>someHtml</body></html>";
	private static final String SOME_STRING = "foobar";
	private RouteResource resource;
	private ConfluencePageTransformer transformer;

	@Before
	public void init() {
		transformer = mock(ConfluencePageTransformer.class);
		resource = new RouteResource(transformer);
	}

	@Test
	public void should_return_html_content() throws Exception {
		when(transformer.grabAndTransform(SOME_STRING)).thenReturn(SOME_HTML);
		assertThat(resource.route(SOME_STRING), equalTo(SOME_HTML));
	}

	@Test
	public void should_testname() throws Exception {

	}

	@Test(expected = NotFoundException.class)
	public void should_throw_404_when_id_is_null() {
		resource.route(null);
	}

	@Test(expected = NotFoundException.class)
	public void should_throw_404_when_id_is_empty() {
		resource.route("");
	}

	@Test(expected = WebApplicationException.class)
	public void should_throw_500_when_mayhem_occurs_down_under() throws Exception {
		when(transformer.grabAndTransform(anyString())).thenThrow(new CantProcessException(new Exception()));
		resource.route(anyString());
	}
}
